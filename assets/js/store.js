'use strict';

var store = new Vuex.Store({
    state: {
        debug: false,

        rate: null,
        goodsCurr:      null,
        goodsBefore:   null,
        cart: []
    },

    mutations: {
        setGoods: function (state, data) {
            state.goodsBefore = state.goodsCurr;
            state.goodsCurr = data;
            if (state.debug) {
                console.debug('goodsBefore', state.goodsBefore);
                console.debug('goodsCurr', state.goodsCurr);
            }
        },

        setRate: function (state, data) {
            state.rate = data;
            if (state.debug) {
                console.debug('rate', state.rate);
            }
        },

        addToCart: function (state, data) {
            var curr = null;
            for (var i in state.cart) {
                if (state.cart[i].id == data.id) {
                    state.cart[i].amount++;
                    curr = i;
                    break;
                }
            }
            if (curr === null) {
                state.cart.push({
                    id: data.id,
                    title: data.title,
                    amount: 1
                });
            }

            if (state.debug) {
                console.debug('add to cart', state.cart);
            }
        },

        removeFromCart: function (state, id) {
            for (var i in state.cart) {
                if (state.cart[i].id == id) {
                    state.cart.splice(i, 1);
                    break;
                }
            }

            if (state.debug) {
                console.debug('remove from cart', state.cart);
            }
        }
    }
});
