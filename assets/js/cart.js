"use strict";
Vue.component('vue-cart', {
    template: '#vue-cart',
    store: store,

    methods: {
        getGoods() {
            return this.$store.state.cart;
        },

        getPrice(id) {
            for (var i in this.$store.state.goodsCurr) {
                if (id == this.$store.state.goodsCurr[i].T) {
                    var val = this.$store.state.rate * this.$store.state.goodsCurr[i].C;
                    return val.toLocaleString('ru')
                }
            }
            return 0;
        },

        removeFromCart(id) {
            store.commit('removeFromCart', id);
        }
    },

    computed: {
        isDisplayed: function() {
            return this.$store.state.cart.length;
        }
    }
});
