"use strict";
var form = new Vue(
    {
        el: '#app',
        store: store,
        data: {
            names: {},
            rateBefore: null,
            rateCurr: null,
            isReady: false
        },
        created: function () {
            var that = this;

            axios
                .get(this.getNamesUrl())
                .then(response => {
                    this.names = response.data;
                    this.isReady = true;
                })
                .catch(e => {
                    alert('Critical error, please contact technical support.');
                    console.log('error: ' + e);
                });

            setInterval(function getGoods() {
                var
                    min = 20,
                    max = 80,
                    rand = min - 0.5 + Math.random() * (max - min + 1)

                that.rateBefore = that.rateCurr;
                that.rateCurr = Math.round(rand);
                store.commit('setRate', that.rateCurr);
                axios
                    .get(that.getGoodsUrl())
                    .then(response => {
                        store.commit('setGoods', response.data.Value.Goods);
                    })
                    .catch(e => {
                        alert('Critical error, please contact technical support.');
                        console.log('error: ' + e);
                    });
                return getGoods;
            }(), 15000);
        },

        methods: {
            getGoodsUrl() {
                return $('#app').data('goods');
            },

            getNamesUrl() {
                return $('#app').data('names');
            },

            isGroupDisplayed(groupId) {

                for (var i in this.$store.state.goodsCurr) {
                    if (this.$store.state.goodsCurr[i].G == groupId) {
                        return true;
                    }
                }
                return false;
            },

            getGoods(groupId) {
                var res = [];
                for (var i in this.$store.state.goodsCurr) {

                    if (this.$store.state.goodsCurr[i].G == groupId) {
                        res.push(this.$store.state.goodsCurr[i]);
                    }
                }
                return res;
            },

            priceRub(val, format, rate) {
                rate = rate || this.rateCurr;
                format = format || false;
                val = val * rate;
                if (format) {
                    return val.toLocaleString('ru');
                }
                return val;
            },

            howToChangePrice(productId) {
                var before = null,
                    current = null;
                for (var i in this.$store.state.goodsBefore) {
                    if (this.$store.state.goodsBefore[i].T == productId) {
                        before = this.priceRub(this.$store.state.goodsBefore[i].C, false, this.rateBefore);
                        break;
                    }
                }

                if (before === null) return 0;

                for (var i in this.$store.state.goodsCurr) {
                    if (this.$store.state.goodsCurr[i].T == productId) {
                        current = this.priceRub(this.$store.state.goodsCurr[i].C);
                        break;
                    }
                }

                if (current === null) return 0;

                return current > before ? 1 : -1;
            },

            addToCart(id, title) {
                store.commit('addToCart', {id: id, title: title});
            }
        }
    }
);